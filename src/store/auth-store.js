import React, { useEffect, useState } from 'react'

const AuthStore = React.createContext({
  isLoggedIn: false,
  onLogout: () => {}, // not mandatory, only useful for IDE & ts
  onLogin: (email, password) => {}, // not mandatory, only useful for IDE & ts
})

export const AuthStoreProvider = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false)

  /**
   * We could add isLoggedIn as dependency but we only need to trigger this effect once, during initialisation
   * if isLoggedIn has to be changed during the app running except for logout event
   */
  useEffect(() => {
    const isUserLoggedInFromLocalStore = localStorage.getItem('isLoggedIn')

    if (isUserLoggedInFromLocalStore === '1') {
      setIsLoggedIn(true)
    }
  }, [])

  const loginHandler = (email, password) => {
    // We should of course check email and password
    // But it's just a dummy/ demo anyways
    localStorage.setItem('isLoggedIn', '1')
    setIsLoggedIn(true)
  }

  const logoutHandler = () => {
    localStorage.removeItem('isLoggedIn')
    setIsLoggedIn(false)
  }

  return (
    <AuthStore.Provider
      value={{
        isLoggedIn: isLoggedIn,
        onLogout: logoutHandler,
        onLogin: loginHandler,
      }}
    >
      {props.children}
    </AuthStore.Provider>
  )
}

export default AuthStore
