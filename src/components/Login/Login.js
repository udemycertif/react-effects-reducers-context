import React, { useState, useEffect, useReducer, useContext, useRef } from 'react'

import Card from '../UI/Card/Card'
import classes from './Login.module.css'
import Button from '../UI/Button/Button'
import AuthStore from '../../store/auth-store'
import Input from '../UI/Input/Input'

/**
 * When to use useReducer ?
 * Obviously, from the moment useState is used, useReducer is usable. Then, we are we going to use
 * one over the other.
 *
 * First, useState should be use until it becomes cumbersome or it starts introducing some
 * buggy/unintended behaviors.
 *
 * useState: main management tool, greate for independent pieces of state / data
 *    great if state updates are easy and limited to a few kinds of updates
 *
 * useReducer: great if more power is needed. Also when dealing with related state updates.
 * Can be helpful when more complex state updates is present. Be careful with it it can be overkill
 *
 */

const emailReducer = (state, action) => {
  console.log('emailReducer')
  switch (action.type) {
    case 'EMAIL_CHANGED':
      console.log('emailReducer ==> EMAIL_CHANGED')
      return {
        ...state,
        value: action.value,
        isValid: action.value.includes('@'),
      }
    case 'EMAIL_INPUT_BLUR':
      console.log('emailReducer ==> EMAIL_INPUT_BLUR')
      return { ...state, isValid: state.value.includes('@') }
    default:
      console.log('emailReducer => default state => ', state)
      return state ? { ...state } : { value: '', isValid: false }
  }
  //return {value: '', isValid: false}
}

const pwdReducer = (state, action) => {
  console.log('pwdReducer')
  switch (action.type) {
    case 'PWD_CHANGED':
      console.log('pwdReducer ==> PWD_CHANGED')
      return {
        ...state,
        value: action.value,
        isValid: action.value.trim().length > 6,
      }
    case 'PWD_INPUT_BLUR':
      console.log('pwdReducer ==> PWD_INPUT_BLUR')
      return { ...state, isValid: state.value.trim().length > 6 }
    default:
      console.log('pwdReducer => default state => ', state)
      return state ? { ...state } : { value: '', isValid: false }
  }
}

const Login = (props) => {
  const authStore = useContext(AuthStore)

  // Replacing useStates w/ useReducer
  // const [enteredEmail, setEnteredEmail] = useState('');
  // const [emailIsValid, setEmailIsValid] = useState();
  // const [enteredPassword, setEnteredPassword] = useState('')
  // const [passwordIsValid, setPasswordIsValid] = useState()
  const [formIsValid, setFormIsValid] = useState(false)

  const [emailState, dispatchEmail] = useReducer(emailReducer, {
    value: '',
    isValid: null,
  })

  const [pwdState, dispatchPwd] = useReducer(pwdReducer, {
    value: '',
    isValid: null,
  })

  const { isValid: isEmailValid } = emailState
  const { isValid: isPwdValid } = pwdState

  const emailInputRef = useRef();
  const pwdInputRef = useRef();

  // Validation with useEffect way
  useEffect(() => {
    const timer = setTimeout(() => {
      console.log('Validity check')
      setFormIsValid(isEmailValid && isPwdValid)
    }, 800)

    return () => {
      console.log('cleanup useEffect')
      clearTimeout(timer)
    }
  }, [isEmailValid, isPwdValid])

  const emailChangeHandler = (event) => {
    // useStates w/out useReducer method
    // setEnteredEmail(event.target.value);

    // Using useReducer
    dispatchEmail({ type: 'EMAIL_CHANGED', value: event.target.value })

    // Validation without useEffect way
    // setFormIsValid(event.target.value.includes('@') && pwdState.isValid)
  }

  const passwordChangeHandler = (event) => {
    // useStates w/out useReducer method
    // setEnteredPassword(event.target.value)

    // Using useReducer
    dispatchPwd({ type: 'PWD_CHANGED', value: event.target.value })

    // Validation without useEffect way
    // setFormIsValid(emailState.isValid && event.target.value.trim().length > 6)
  }

  const validateEmailHandler = () => {
    // Using useReducer
    dispatchEmail({ type: 'EMAIL_INPUT_BLUR' })

    // useStates w/out useReducer method
    // setEmailIsValid(emailState.isValid);
  }

  const validatePasswordHandler = () => {
    dispatchPwd({ type: 'PWD_INPUT_BLUR' })
    // useStates w/out useReducer method
    // setPasswordIsValid(enteredPassword.trim().length > 6)
  }

  const submitHandler = (event) => {
    event.preventDefault()
    if(formIsValid) {
      authStore.onLogin(emailState.value, pwdState.value)
    } else if(!isEmailValid) {
      emailInputRef.current.focus()
    } else {
      pwdInputRef.current.focus()
    }
  }

  return (
    <Card className={classes.login}>
      <form onSubmit={submitHandler}>
        <Input
          ref={emailInputRef}
          id="email"
          type="email"
          label="E-Mail"
          isValid={emailState.isValid}
          value={emailState.value}
          onChange={emailChangeHandler}
          onBlur={validateEmailHandler}
        />
        <Input
          ref={pwdInputRef}
          id="password"
          type="password"
          label="Password"
          isValid={pwdState.isValid}
          value={pwdState.value}
          onChange={passwordChangeHandler}
          onBlur={validatePasswordHandler}
        />
        <div className={classes.actions}>
          <Button type="submit" className={classes.btn}> {/* disabled={!formIsValid} */}
            Login
          </Button>
        </div>
      </form>
    </Card>
  )
}

export default Login
