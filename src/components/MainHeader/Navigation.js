import React, {useContext}  from 'react'
import AuthStore from '../../store/auth-store'

import classes from './Navigation.module.css'

const Navigation = () => {
  const ctxt = useContext(AuthStore);

  return (
    // One way to consume isLoggedIn state from auth store
          <nav className={classes.nav}>
            <ul>
              {ctxt.isLoggedIn && (
                <li>
                  <a href="/">Users</a>
                </li>
              )}
              {ctxt.isLoggedIn && (
                <li>
                  <a href="/">Admin</a>
                </li>
              )}
              {ctxt.isLoggedIn && (
                <li>
                  <button onClick={ctxt.onLogout}>Logout</button>
                </li>
              )}
            </ul>
          </nav>
  )
}

export default Navigation
