import React, { useRef, useImperativeHandle } from 'react'
import classes from './Input.module.css'

const Input = React.forwardRef(({ id, type, label, isValid, value, onChange, onBlur }, ref) => {
  const inputRef = useRef()

  // learning "forward Ref", we want activate to be called outside of the input component
  // WARNING: really rare case, not always a good way to do things
  const activate = () => {
      inputRef.current.focus();
  }
  useImperativeHandle(ref, () => {
      return {focus: activate}
  })

  return (
    <div
      className={`${classes.control} ${
        isValid === false ? classes.invalid : ''
      }`}
    >
      <label htmlFor={id}>{label}</label>
      <input
        ref={inputRef}
        type={type}
        id={id}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
    </div>
  )
})

export default Input
