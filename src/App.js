import React, { useContext } from 'react'

import Login from './components/Login/Login'
import Home from './components/Home/Home'
import MainHeader from './components/MainHeader/MainHeader'
import AuthStore from './store/auth-store'

function App() {
  const authStore = useContext(AuthStore)

  return (
    <React.Fragment>
        <MainHeader />
        <main>
          {!authStore.isLoggedIn && <Login />}
          {authStore.isLoggedIn && <Home />}
        </main>
    </React.Fragment>
  )
}

export default App
